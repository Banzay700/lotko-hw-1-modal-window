import React, { Component } from 'react'
import cn from 'classnames'
import s, { overlay, modal, title, close } from './Modal.module.scss'

interface ModalProps {
  header: string
  isCloseButton: boolean
  text: string
  actions: React.ReactNode
  openModal: boolean
  closeModal: () => void
}

class Modal extends Component<ModalProps> {
  render() {
    const { header, text, isCloseButton, actions, openModal, closeModal } = this.props

    return (
      <>
        {openModal && (
          <>
            <div className={overlay} onClick={closeModal} />
            <div className={modal}>
              <div className={title}>{header}</div>
              {isCloseButton && <i className={cn('ri-close-fill', close)} onClick={closeModal} />}
              <div>{text}</div>
              <div className={s['modal-actions']}>{actions}</div>
            </div>
          </>
        )}
      </>
    )
  }
}

export default Modal
