import { Component } from 'react'
import { Home } from './pages'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Home />
      </div>
    )
  }
}

export default App
