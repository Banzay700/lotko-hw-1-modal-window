import { Component } from 'react'
import cn from 'classnames'

import { button, accent } from './Button.module.scss'

interface ButtonProps {
  text: string
  bgColor?: boolean
  onClick?: () => void
}

class Button extends Component<ButtonProps> {
  render() {
    const { text, bgColor, onClick } = this.props

    return (
      <button className={cn(button, bgColor && accent)} onClick={onClick} type="button">
        {text}
      </button>
    )
  }
}

export default Button
