import { Component } from 'react'
import { Modal } from 'src/components'
import { Button } from 'src/UI'
import { ModalProps, offerModalProps, surveyModalProps } from './home.utils'

import { home, actions } from './Home.module.scss'

interface HomeState {
  isOpenModal: boolean
  modalType: ModalProps
}

class Home extends Component<any, HomeState> {
  state = {
    isOpenModal: false,
    modalType: offerModalProps,
  }

  handlerShowModal = (type: ModalProps) => {
    this.setState({ isOpenModal: true, modalType: type })
  }

  handlerCloseModal = () => {
    this.setState({ isOpenModal: false })
  }

  render() {
    const { isOpenModal, modalType } = this.state

    return (
      <div className={home}>
        <Modal openModal={isOpenModal} closeModal={this.handlerCloseModal} {...modalType} />
        <div className={actions}>
          <Button text="Claim Now" bgColor onClick={() => this.handlerShowModal(offerModalProps)} />
          <Button text="Take Survey" onClick={() => this.handlerShowModal(surveyModalProps)} />
        </div>
      </div>
    )
  }
}

export default Home
