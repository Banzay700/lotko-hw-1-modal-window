import { Button } from 'src/UI'
import { ReactNode } from 'react'

const actions = (
  <>
    <Button text="Ok" />
    <Button text="Close" />
  </>
)

export interface ModalProps {
  header: string
  isCloseButton: boolean
  text: string
  actions: ReactNode
}

export const offerModalProps = {
  header: 'Special Offer',
  isCloseButton: true,
  text: "Congratulations! You've won a special offer. Click the button below to claim your prize. But hurry, this offer is only available for the next 24 hours!",
  actions,
}

export const surveyModalProps = {
  header: 'Survey',
  isCloseButton: false,
  text: " We'd love to hear your feedback on our product. Please take a moment to complete our short survey and let us know how we're doing. Your input is greatly appreciated.",
  actions,
}
